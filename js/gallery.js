galleryDef = [	// Slideshow Images
		{image : 'images/bg_slider/fullscreen_slider1.jpg', title : '<span>digital era</span> aqui encontrara las mejores marcas, garantia y respaldo como sus aliados en suministros de alta tecnologia para su hogar u oficina<a href="#"></a>', thumb : 'images/bg_slider/spacer.gif', url : ''},
		{image : 'images/bg_slider/fullscreen_slider2.jpg', title : '<span>digital era</span> Fusce id sapien dolor, nec commodo arcu. Ut isus, commodo in gravida in, vehicula <a href="#">read more...</a>', thumb : 'images/bg_slider/spacer.gif', url : ''},
		{image : 'images/bg_slider/fullscreen_slider3.jpg', title : '<span>risus Ut</span> Fusce id sapien dolor, nec commodo arcu. Ut isus, commodo in gravida in, vehicula <a href="#">read more...</a>', thumb : 'images/bg_slider/spacer.gif', url : ''},	
		{image : 'images/bg_slider/fullscreen_slider4.jpg', title : '<span>Fusce nec</span> Fusce id sapien dolor, nec commodo arcu. Ut isus, commodo in gravida in, vehicula <a href="#">read more...</a>', thumb : 'images/bg_slider/spacer.gif', url : ''},	
		{image : 'images/bg_slider/fullscreen_slider5.jpg', title : '<span>gravida commodo</span> Fusce id sapien dolor, nec commodo arcu. Ut isus, commodo in gravida in, vehicula <a href="#">read more...</a>', thumb : 'images/bg_slider/spacer.gif', url : ''},	
		{image : 'images/bg_slider/fullscreen_slider6.jpg', title : '<span>vehicula id</span> Fusce id sapien dolor, nec commodo arcu. Ut isus, commodo in gravida in, vehicula <a href="#">read more...</a>', thumb : 'images/bg_slider/spacer.gif', url : ''},	
		{image : 'images/bg_slider/fullscreen_slider7.jpg', title : '<span>enim elit</span> Fusce id sapien dolor, nec commodo arcu. Ut isus, commodo in gravida in, vehicula <a href="#">read more...</a>', thumb : 'images/bg_slider/spacer.gif', url : ''}
	];

jQuery(function($){
			
	$.supersized({
    	// Functionality
		slideshow               :   1,			// Slideshow on/off
		autoplay				:	1,			// Slideshow starts playing automatically
		start_slide             :   1,			// Start slide (0 is random)
		stop_loop				:	0,			// Stops slideshow on last slide
		random					: 	0,			// Randomize slide order (Ignores start slide)
		slide_interval          :   8000,		// Length between transitions
		transition              :   1, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
		transition_speed		:	1000,		// Speed of transition
		new_window				:	1,			// Image links open in new window/tab
		pause_hover             :   0,			// Pause slideshow on hover
		keyboard_nav            :   1,			// Keyboard navigation on/off
		performance				:	2,			// 0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed //  (Only works for Firefox/IE, not Webkit)
		image_protect			:	1,			// Disables image dragging and right click with Javascript
												   
		// Size & Position
		fit_always				:	0,			// Image will never exceed browser width or height (Ignores min. dimensions)
		fit_landscape			:   0,			// Landscape images will not exceed browser width
		fit_portrait         	:   1,			// Portrait images will not exceed browser height  			   
		min_width		        :   100,			// Min width allowed (in pixels)
		min_height		        :   100,			// Min height allowed (in pixels)
		horizontal_center       :   0,			// Horizontally center background
		vertical_center         :   0,			// Vertically center background
		
												   
		// Components							
		slide_links				:	0,			// Individual links for each slide (Options: false, 'num', 'name', 'blank')
		thumb_links				:	0,			// Individual thumb links for each slide
		thumbnail_navigation    :   0,			// Thumbnail navigation
		slides 					:  	galleryDef
		
	});
});